package factoryMethod;

import java.util.Optional;

public class CarFactory {

    public Optional<Car> createCar(Integer budget, boolean isElectric) {
        if (isElectric) {
                if (budget >= 12000 && budget<= 20000) {
                    return Optional.of(new TeslaS());
                }else if (budget >= 20000){
                    return Optional.of(new TeslaX());
                }else return Optional.empty();

        }else{
            if (budget>= 10000 && budget<= 15000){
                return Optional.of(new AudiR2());
            }else if (budget>= 15000){
                return Optional.of(new AudiR8());
            }
            else return Optional.empty();
        }
    }
}
