package factoryMethod;

public class Main {
    public static void main(String[] args) {
        CarFactory carFactory = new CarFactory();
        System.out.println(carFactory.createCar(17000, true));
    }
}
