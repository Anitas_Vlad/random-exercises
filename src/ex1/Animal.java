package ex1;

public abstract class Animal {
    private String name;
    private Integer age;

    public String getName(){
        return name;
    }

    public abstract Integer getAge();
}
