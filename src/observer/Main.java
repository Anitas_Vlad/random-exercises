package observer;

public class Main {
    public static void main(String[] args) {

        Subject subject1 = new Subject(20);
        Observer observer1 = new Observer();
        Observer observer2 = new Observer();
        CustomObserver customObserver1 = new CustomObserver(15);
        CustomObserver customObserver2 = new CustomObserver(80);

        observer1.observe(subject1);
        observer2.observe(subject1);
        customObserver1.observe(subject1);
        customObserver2.observe(subject1);

        observer1.reactToSubject();
        observer2.reactToSubject();
        customObserver1.reactToSubject();
        customObserver2.reactToSubject();
    }
}
