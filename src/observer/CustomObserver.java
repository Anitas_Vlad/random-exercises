package observer;

public class CustomObserver extends Observer {

    private Integer leastToLike;

    public CustomObserver(Integer leastToLike) {
        this.leastToLike = leastToLike;
    }

    @Override
    public void reactToSubject() {
        if (subject.getState() < leastToLike) {
            System.out.println(name + ": Nope");
        } else System.out.println(name + ": Yes");
    }
}
