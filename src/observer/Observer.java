package observer;

import java.util.HashSet;

public class Observer {

    protected String name;
    protected Subject subject;

    public void observe(Subject subject){
        subject.registerAsObserver(this);
        this.subject = subject;
    }

    public void reactToSubject(){
        if (subject.getState()<20){
            System.out.println(name + ": Kinda low");
        }else if (subject.getState() < 50){
            System.out.println(name + ": Good enough");
        }else System.out.println(name + ": Not good");
    }
}
