package observer;

import java.util.HashSet;
import java.util.Set;

public class Subject {
    protected Integer state;
    private Set<Observer> observers;

    public Subject(Integer state) {
        this.state = state;
        this.observers = new HashSet<>();
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Integer getState() {
        return state;
    }

    public void registerAsObserver(Observer observer){
        observers.add(observer);
    }

    public void changeState(){
        this.setState(30);
        observers.forEach(Observer::reactToSubject);
    }
}
