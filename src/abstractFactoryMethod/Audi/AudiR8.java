package abstractFactoryMethod.Audi;

import abstractFactoryMethod.Car;

public class AudiR8 extends Car {

    public AudiR8() {
        super("Audi R8");
    }
}
