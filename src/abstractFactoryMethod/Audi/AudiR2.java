package abstractFactoryMethod.Audi;

import abstractFactoryMethod.Car;

public class AudiR2 extends Car {

    public AudiR2() {
        super("Audi R2");
    }

}
