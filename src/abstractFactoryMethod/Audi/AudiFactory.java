package abstractFactoryMethod.Audi;

import abstractFactoryMethod.Car;
import abstractFactoryMethod.Tesla.TeslaS;
import abstractFactoryMethod.Tesla.TeslaX;

import java.util.Optional;

public class AudiFactory {

    public Optional<Car> createCar(Integer budget){
        if (budget >= 12000 && budget<= 20000) {
            return Optional.of(new AudiR2());
        }else if (budget >= 20000){
            return Optional.of(new AudiR8());
        }else return Optional.empty();
    }
}
