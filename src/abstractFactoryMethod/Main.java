package abstractFactoryMethod;

import factoryMethod.CarFactory;

public class Main {
    public static void main(String[] args) {

        CarDealer carDealer = new CarDealer();
        System.out.println(carDealer.orderCar(17000, false));

    }
}
