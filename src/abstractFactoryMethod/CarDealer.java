package abstractFactoryMethod;

import abstractFactoryMethod.Audi.AudiFactory;
import abstractFactoryMethod.Tesla.TeslaFactory;

import java.util.Optional;

public class CarDealer {

    private TeslaFactory teslaFactory;
    private AudiFactory audiFactory;

    public CarDealer() {
        this.teslaFactory = new TeslaFactory();
        this.audiFactory = new AudiFactory();
    }

    public Optional<Car> orderCar(Integer budget, boolean isElectric) {
        if (isElectric) {
            return teslaFactory.createCar(budget);
        } else return audiFactory.createCar(budget);

    }
}
