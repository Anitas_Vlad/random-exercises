package abstractFactoryMethod.Tesla;

import abstractFactoryMethod.Car;

public class TeslaX extends Car {

    public TeslaX() {
        super("Tesla X");
    }
}
