package abstractFactoryMethod.Tesla;

import abstractFactoryMethod.Car;

public class TeslaS extends Car {

    public TeslaS() {
        super( "Tesla S");
    }
}
