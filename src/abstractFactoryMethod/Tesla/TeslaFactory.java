package abstractFactoryMethod.Tesla;

import abstractFactoryMethod.Audi.AudiR2;
import abstractFactoryMethod.Audi.AudiR8;
import abstractFactoryMethod.Car;

import java.util.Optional;

public class TeslaFactory {
    public Optional<Car> createCar(Integer budget) {
        if (budget >= 10000 && budget <= 15000) {
            return Optional.of(new TeslaS());
        } else if (budget >= 15000) {
            return Optional.of(new TeslaX());
        } else return Optional.empty();
    }
}
